Given(/^student Juan$/) do
  @studentJuan = Student.new("Juan")
end

Given(/^individual task ta$/) do
  @taskTA = IndividualTask.new("ta")
  @studentJuan.assign_individual_task(@taskTA)
end

Given(/^individual task tb$/) do
  @taskTB = IndividualTask.new("tb")
  @studentJuan.assign_individual_task(@taskTB)
end

Given(/^individual task tc$/) do
  @taskTC = IndividualTask.new("tc")
  @studentJuan.assign_individual_task(@taskTC)
end

Given(/^group project pg$/) do
  @projectPG = GroupProject.new("pg")
  @studentJuan.assign_group_project(@projectPG)
end

Given(/^regular student Juan$/) do
end

When(/^Juan passed all individual tasks$/) do
  @studentJuan.pass_individual_task("ta")
  @studentJuan.pass_individual_task("tb")
  @studentJuan.pass_individual_task("tc")
end

When(/^Juan attended at least (\d+)% of the classes$/) do |attendance_percentage|
  @studentJuan.attendance_percentage = attendance_percentage.to_i
end

When(/^Juan passed at least (\d+) iterations of the group project$/) do |passing_group_project_iterations|
  @studentJuan.pass_group_project_iterations(passing_group_project_iterations.to_i)
end

Then(/^Juan passed the course$/) do
  expect(@studentJuan.passing_course?).to eq true
end

When(/^Juan attended (\d+)% of the classes$/) do |attendance_percentage|
  @studentJuan.attendance_percentage = attendance_percentage.to_i
end

Then(/^Juan failed the course$/) do
  expect(@studentJuan.passing_course?).to eq false
end

When(/^Juan failed individual task tc$/) do
  @studentJuan.fail_individual_task("tc")
end

When(/^passed (\d+) iterations of the group project$/) do |passing_group_project_iterations|
  @studentJuan.pass_group_project_iterations(passing_group_project_iterations.to_i)
end
