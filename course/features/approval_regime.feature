Feature: Approval regime

  Background:
    Given student Juan
    Given individual task ta
    Given individual task tb
    Given individual task tc
    Given group project pg

  Scenario: Student passes
    Given regular student Juan
    When Juan passed all individual tasks
    And Juan attended at least 75% of the classes
    And Juan passed at least 3 iterations of the group project
    Then Juan passed the course

  Scenario: Student fails due to low attendance
    Given regular student Juan
    When Juan passed all individual tasks
    And Juan passed at least 3 iterations of the group project
    But Juan attended 60% of the classes
    Then Juan failed the course

  Scenario: Student fails due to individual tasks
    Given regular student Juan
    When Juan attended at least 75% of the classes
    But Juan failed individual task tc
    Then Juan failed the course

  Scenario: Student fails due to project group
    Given regular student Juan
    When Juan passed all individual tasks
    And Juan attended at least 75% of the classes
    And passed 2 iterations of the group project
    Then Juan failed the course
