class GroupProject
  attr_reader :name
  attr_accessor :passing_iterations
  
  def initialize(project_name)
    @name = project_name
    @passing_iterations = 0
  end

end
