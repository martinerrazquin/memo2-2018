class Student

  MINIMUM_PASSING_ATTENDANCE_PERCENTAGE = 75
  MINIMUM_PASSING_PROJECT_ITERATIONS = 3

  attr_reader :name
  attr_accessor :attendance_percentage

  def initialize(name)
    @name = name
    @assigned_individual_tasks = Hash.new
    @assigned_group_project = nil
    @attendance_percentage = 0
  end

  def assigned_tasks
    @assigned_individual_tasks.keys
  end

  def assign_individual_task(task)
    @assigned_individual_tasks[task.name] = task
  end

  def assigned_project?
    !@assigned_group_project.nil?
  end

  def assign_group_project(project)
    @assigned_group_project = project
  end

  def passing_task?(task_name)
    @assigned_individual_tasks[task_name].passing?
  end

  def pass_individual_task(task_name)
    @assigned_individual_tasks[task_name].pass
  end

  def passing_group_project_iterations
    @assigned_group_project.passing_iterations
  end

  def pass_group_project_iterations(passing_iterations)
    @assigned_group_project.passing_iterations= passing_iterations
  end

  def passing_course?
    passing_attendance = passing_attendance?
    passing_individual_tasks = passing_tasks?
    passing_group_project = passing_project?
    passing_attendance && passing_individual_tasks && passing_group_project
  end

  private def passing_attendance?
    @attendance_percentage >= MINIMUM_PASSING_ATTENDANCE_PERCENTAGE
  end

  def fail_individual_task(task_name)
    @assigned_individual_tasks[task_name].fail
  end

  private def passing_tasks?
    passing = true

    @assigned_individual_tasks.each_value do |individual_task|
      passing &= individual_task.passing?
    end

    passing
  end

  private def passing_project?
    if !assigned_project?
      return true
    end

    @assigned_group_project.passing_iterations >= MINIMUM_PASSING_PROJECT_ITERATIONS
  end

end
