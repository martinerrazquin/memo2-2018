class IndividualTask
  attr_reader :name

  def initialize(task_name)
    @name = task_name
    @passing = false
  end

  def passing?
    @passing
  end

  def pass
    @passing = true
  end

  def fail
    @passing = false
  end
end
