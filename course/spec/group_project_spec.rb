require 'rspec'
require_relative '../model/group_project'

describe 'GroupProject' do

  let(:project_name) { "p" }
  let(:project) { GroupProject.new(project_name) }

  it 'project named p should give "p" when asked for its name' do
    expect(project.name).to eq project_name
  end

  it 'new project should have 0 passing iterations' do
    expect(project.passing_iterations).to eq 0
  end

  it 'passing 5 iterations of a project should make it have 5 passing iterations' do
    passing_iterations = 5
    project.passing_iterations= passing_iterations
    expect(project.passing_iterations).to eq passing_iterations
  end
end
