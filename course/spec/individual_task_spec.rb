require 'rspec'
require_relative '../model/individual_task'

describe 'IndividualTask' do

  let(:task_name) { "t" }
  let(:task) { IndividualTask.new(task_name) }

  it 'task named t should give "t" when asked for its name' do
    expect(task.name).to eq task_name
  end

  it 'new task should not be passing' do
    expect(task.passing?).to eq false
  end

  it 'passing a task should make it pass' do
    task.pass
    expect(task.passing?).to eq true
  end

  it 'failing a task should make it fail'do
    task.fail
    expect(task.passing?).to eq false
  end

end
