require 'rspec'
require_relative '../model/student'
require_relative '../model/individual_task'
require_relative '../model/group_project'

describe 'Student' do

  let(:student_name) { "Juan" }
  let(:student) { Student.new(student_name) }

  it 'student named Juan should give "Juan" when asked for his/her name' do
    expect(student.name).to eq student_name
  end

  it 'newly created student should have no assigned individual tasks' do
    expect(student.assigned_tasks).to eq []
  end

  it 'assigning a task called "t" to a student should be included in his/her assigned tasks' do
    task_name1 = "t"
    t = IndividualTask.new(task_name1)
    student.assign_individual_task(t)
    expect(student.assigned_tasks).to include(task_name1)
  end

  it 'newly created student should not have an assigned group project' do
    expect(student.assigned_project?).to eq false
  end

  it 'assigning a project to a student should make him have an assigned group project' do
    p = GroupProject.new("p")
    student.assign_group_project(p)
    expect(student.assigned_project?).to eq true
  end

  it 'newly created and assigned task should not be passing' do
    task_name1 = "t"
    t = IndividualTask.new(task_name1)
    student.assign_individual_task(t)
    expect(student.passing_task?("t")).to eq false
  end

  it 'passing a task should make it pass' do
    task_name1 = "t"
    t = IndividualTask.new(task_name1)
    student.assign_individual_task(t)
    student.pass_individual_task("t")
    expect(student.passing_task?("t")).to eq true
  end

  it 'new student should have 0 attendance percentage' do
    expect(student.attendance_percentage).to eq 0
  end

  it 'student with 50 percent attendance should have a 50 attendance percentage' do
    attendance = 50
    student.attendance_percentage= attendance
    expect(student.attendance_percentage).to eq attendance
  end

  it 'a project just created and assigned should have 0 passing iterations' do
    p = GroupProject.new("p")
    student.assign_group_project(p)
    expect(student.passing_group_project_iterations).to eq 0
  end

  it 'passing 3 iterations of the assigned project should make it have 3 passing iterations' do
    passing_iterations = 3
    p = GroupProject.new("p")
    student.assign_group_project(p)
    student.pass_group_project_iterations(passing_iterations)
    expect(student.passing_group_project_iterations).to eq passing_iterations
  end

  it 'new student should not be passing course' do
    expect(student.passing_course?).to eq false
  end

  it 'student with 75+ attendance percentage and no assigned tasks nor project should pass course' do
    student.attendance_percentage= 75
    expect(student.passing_course?).to eq true
  end

  it 'failing a task should make it failed, even if it was passing before' do
    task_name1 = "t"
    t = IndividualTask.new(task_name1)
    student.assign_individual_task(t)
    student.pass_individual_task("t")
    student.fail_individual_task("t")
    expect(student.passing_task?("t")).to eq false
  end

  it 'failing a task should make the student not pass the course, even if he had passing attendance percentage' do
    task_name1 = "t"
    t = IndividualTask.new(task_name1)
    student.assign_individual_task(t)
    student.fail_individual_task("t")

    student.attendance_percentage= 80

    expect(student.passing_course?).to eq false
  end

  it 'passing only 2 iterations of group project should make the student not pass course, even if passing attendance' do
    passing_iterations = 2
    p = GroupProject.new("p")
    student.assign_group_project(p)
    student.pass_group_project_iterations(passing_iterations)

    student.attendance_percentage= 80

    expect(student.passing_course?).to eq false
  end
end
