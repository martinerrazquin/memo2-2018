require File.expand_path '../spec_helper.rb', __FILE__

describe 'Sinatra App' do

  SUM_PREFIX = '/sum'
  CHOP_PREFIX = '/chop'

  it 'sumar 0 y 0 debe ser una operacion valida' do
    summand = '0,0'
    get SUM_PREFIX, {'x': summand}

    expect(last_response).to be_ok
  end

  it 'sumar 0 y 0 debe informar que suma 0 y 0 en la respuesta' do
    summand = '0,0'
    get SUM_PREFIX, {'x': summand}

    response = JSON.parse(last_response.body)
    expect(response['sum']).to eq summand
  end

  it 'sumar 0 y 0 debe informar "cero" como resultado en la respuesta' do
    summand = '0,0'
    get SUM_PREFIX, {'x': summand}

    response = JSON.parse(last_response.body)
    expect(response['resultado']).to eq "cero"
  end

  it 'sumar 1,2,4 debe informar "1,2,4" como suma' do
    summand = "1,2,4"
    get SUM_PREFIX, {'x': summand}

    response = JSON.parse(last_response.body)
    expect(response['sum']).to eq summand
  end

  it 'sumar 1,2,3 debe informar "seis" como resultado' do
    summand = '1,2,3'
    get SUM_PREFIX, {'x': summand}

    response = JSON.parse(last_response.body)
    expect(response['resultado']).to eq "seis"
  end

  it 'sumar 50,50 debe informar "demasiado grande" como resultado' do
    summand = '50,50'
    get SUM_PREFIX, {'x': summand}

    response = JSON.parse(last_response.body)
    expect(response['sum']).to eq summand
    expect(response['resultado']).to eq "demasiado grande"
  end

  it 'no sumar nada debe informar "vacio" como resultado' do
    get SUM_PREFIX

    response = JSON.parse(last_response.body)
    expect(response['sum']).to eq ""
    expect(response['resultado']).to eq "vacio"
  end

  it 'chopear 3 en 0,7,3 debe ser una operacion valida' do
    element = "3"
    array = "0,7,3"
    post CHOP_PREFIX, {'x': element, 'y': array}

    expect(last_response).to be_ok
  end

  it 'chopear 2 en 0,7,3 debe informar "x=2,y=0,7,3" como chop en la respuesta' do
    element = "2"
    array = "0,7,3"
    post CHOP_PREFIX, {'x': element, 'y': array}

    response = JSON.parse(last_response.body)
    expect(response['chop']).to eq CHOP_PARAM_RESPONSE_FORMAT % {x: element, y: array}
  end

  it 'chopear 3 en 0,7,3 debe informar 2 como resultado' do
    element = "3"
    array = "0,7,3"
    post CHOP_PREFIX, {'x': element, 'y': array}

    response = JSON.parse(last_response.body)
    expect(response['resultado']).to eq 2
  end

  it 'chopear 1 en 3,3,3 debe informar "x=1,y=3,3,3" como chop en la respuesta' do
    element = "1"
    array = "3,3,3"
    post CHOP_PREFIX, {'x': element, 'y': array}

    response = JSON.parse(last_response.body)
    expect(response['chop']).to eq CHOP_PARAM_RESPONSE_FORMAT % {x: element, y: array}
  end

  it 'chopear 5 en 0,7,3,1 debe informar -1 como resultado' do
    element = "5"
    array = "0,7,3,1"
    post CHOP_PREFIX, {'x': element, 'y': array}

    response = JSON.parse(last_response.body)
    expect(response['resultado']).to eq -1
  end

  it 'chop de 3 en vacio debe devolver -1 como resultado y mostrar "x=3,y=" como chop en la respuesta' do
    element = "3"
    array = ""
    post CHOP_PREFIX, {'x': element, 'y': array}

    response = JSON.parse(last_response.body)
    expect(response['chop']).to eq CHOP_PARAM_RESPONSE_FORMAT % {x: element, y: array}
    expect(response['resultado']).to eq -1
  end

end
