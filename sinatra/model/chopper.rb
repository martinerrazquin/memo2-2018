class Chopper

  def chop(n, array)
		array.each_with_index do |elemento,indiceElemento|
			if elemento == n
				return indiceElemento
			end
		end
    return -1
  end

	def sum(array)
		if array.length == 0
			return 'vacio'
		end

		suma = 0
		array.each {|elemento| suma += elemento}
		
		palabra = ''
		if suma >= 100
			return 'demasiado grande'
		end
		if suma >= 10
			palabra += cifra_a_palabra((suma / 10) % 10) + ','
		end
		palabra += cifra_a_palabra(suma % 10)
		
		return palabra
	end

end

=begin
Recibe un digito y devuelve su pronunciacion en castellano, si no es un digito devuelve una cadena vacia.
=end
def cifra_a_palabra(cifra)
	case cifra
		when 0
			return 'cero'
		when 1
			return 'uno'
		when 2
			return 'dos'
		when 3
			return 'tres'
		when 4
			return 'cuatro'
		when 5
			return 'cinco'
		when 6
			return 'seis'
		when 7
			return 'siete'
		when 8
			return 'ocho'
		when 9
			return 'nueve'
		end
	return ''
end
