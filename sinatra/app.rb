require 'sinatra'
require 'json'
require_relative 'model/chopper'

DEFAULT_SUM_PARAMETER = ""
CHOP_PARAM_RESPONSE_FORMAT = 'x=%{x},y=%{y}'

get '/sum' do
  array = params['x'] || DEFAULT_SUM_PARAMETER
  num_array = array.split(',').map(&:to_i)

  chopper = Chopper.new
  resultado = chopper.sum(num_array)

  content_type :json
  { :sum => array, :resultado => resultado }.to_json
end

post '/chop' do
  x = params['x']
  y = params['y']
  num = x.to_i
  array = y.split(',').map(&:to_i)

  chopper = Chopper.new
  resultado = chopper.chop(num, array)
  
  chop_param_response = CHOP_PARAM_RESPONSE_FORMAT % {x: x, y: y}
  content_type :json
  { :chop => chop_param_response, :resultado => resultado }.to_json
end
