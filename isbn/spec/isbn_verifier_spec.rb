require 'rspec'
require_relative '../model/isbn_verifier'

describe 'IsbnVerifier' do

  let (:verifier) { IsbnVerifier.new }

  it 'empty string should be invalid' do
    expect(verifier.verify('')).to eq false
  end

  it '0000000000 (ten zeros) should be valid' do
    expect(verifier.verify('0000000000')).to eq true
  end

  it "00X0000000 should be invalid" do
    expect(verifier.verify('00X0000000')).to eq false
  end

  it "00 00000000 should be valid" do
    expect(verifier.verify('00 00000000')).to eq true
  end

  it "0000000-000 should be valid" do
    expect(verifier.verify('0000000-000')).to eq true
  end

  it "1111111118 should be invalid" do
    expect(verifier.verify('1111111119')).to eq false
  end

  it "00000000000 should be invalid" do
    expect(verifier.verify('00000000000')).to eq false
  end

  it "100000001X should be valid" do
      expect(verifier.verify('100000001X')).to eq true
  end

  it "101111111X should be valid" do
      expect(verifier.verify('101111111X')).to eq true
  end
end
