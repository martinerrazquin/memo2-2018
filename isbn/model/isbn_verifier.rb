class IsbnVerifier

  def verify(str)
    str.delete!(' -')
    if (str =~ /\A\d{9}(\d|X)\z/) == nil
      return false
    end

    verifying_digit = str[-1]
    case verifying_digit
    when 'X'
      expected_sum = 10
    else
      expected_sum = verifying_digit.to_i
    end

    summing_digits = str.chop
    sum = sum_products_of_index_and_digit_from_string(summing_digits) % 11
    return sum == expected_sum
  end

  private def sum_products_of_index_and_digit_from_string(str)
    sum = 0
    (0..str.length).each do |indx|
      sum = sum + str[indx].to_i*(indx+1)
    end
    return sum
  end

end
