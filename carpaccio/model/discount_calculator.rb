class DiscountCalculator
  TAX_PERCENTAGE_BY_STATE = {'NV' => 8.0, 'UT' => 6.85, 'TX' => 6.25, 'AL' => 4.0, 'CA' => 8.25}
  DISCOUNT_PERCENTAGE_0_TO_1k = 0
  DISCOUNT_PERCENTAGE_1k_TO_5k = 3
  DISCOUNT_PERCENTAGE_5k_TO_7k = 5
  DISCOUNT_PERCENTAGE_7k_TO_10k = 7
  DISCOUNT_PERCENTAGE_10k_TO_50k = 10
  DISCOUNT_PERCENTAGE_ABOVE_50k = 15

  def calculate_total(unit_price, quantity, state)
    total_price = unit_price * quantity

    discounted_price = discount_by_thresholds(total_price)

    taxed_price = add_tax_by_state(discounted_price,state)

    taxed_price
  end

  private def add_tax_by_state(price,state)
    price * (100 + TAX_PERCENTAGE_BY_STATE[state])/100
  end

  private def discount_by_thresholds(price)
    discount_percentage =
      case price
      when 0...1000 then DISCOUNT_PERCENTAGE_0_TO_1k
      when 1000...5000 then DISCOUNT_PERCENTAGE_1k_TO_5k
      when 5000...7000 then DISCOUNT_PERCENTAGE_5k_TO_7k
      when 7000...10000 then DISCOUNT_PERCENTAGE_7k_TO_10k
      when 10000...50000 then DISCOUNT_PERCENTAGE_10k_TO_50k
      else DISCOUNT_PERCENTAGE_ABOVE_50k
      end

    price * (100 - discount_percentage)/100
  end
end
