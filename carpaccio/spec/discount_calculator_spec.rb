require 'rspec'
require_relative '../model/discount_calculator'

describe 'DiscountCalculator' do

  let(:calculator) { DiscountCalculator.new }

  it 'total de 1 unidad de precio 0 en NV debe devolver 0' do
    unit_price = 0
    quantity = 1
    state = 'NV'
    expected_total = 0

    expect(calculator.calculate_total(unit_price,quantity,state)).to eq expected_total
  end

  it 'total de 1 unidad de precio 100 en NV debe devolver 108' do
    unit_price = 100
    quantity = 1
    state = 'NV'
    expected_total = 108

    expect(calculator.calculate_total(unit_price,quantity,state)).to eq expected_total
  end

  it 'total de 2 unidades de precio 100 en NV debe devolver 216' do
    unit_price = 100
    quantity = 2
    state = 'NV'
    expected_total = 216

    expect(calculator.calculate_total(unit_price,quantity,state)).to eq expected_total
  end

  it 'total de 1 unidad de precio 1500 en NV debe devolver 1571.4' do
    unit_price = 1500
    quantity = 1
    state = 'NV'
    expected_total = 1571.4

    expect(calculator.calculate_total(unit_price,quantity,state)).to eq expected_total
  end

  it 'total de 1 unidad de precio 100 en UT debe devolver 106.85' do
    unit_price = 100
    quantity = 1
    state = 'UT'
    expected_total = 106.85

    expect(calculator.calculate_total(unit_price,quantity,state)).to eq expected_total
  end

  it 'total de 1 unidad de precio unitario 50000 en Alabama debe devolver 44200' do
    unit_price = 50000
    quantity = 1
    state = 'AL'
    expected_total = 44200

    expect(calculator.calculate_total(unit_price,quantity,state)).to eq expected_total
  end

end
