require File.expand_path '../spec_helper.rb', __FILE__

describe 'Sinatra App' do

  it 'total de 1 unidad de precio unitario 0 en Nevada debe ser una respuesta valida' do
    price = 0
    get GET_TOTAL_PREFIX, {'quantity':1, "price":price,"state":"NV"}

    expect(last_response).to be_ok
  end

  it 'total de 1 unidad de precio unitario 0 en Nevada debe devolver 0' do
    price = 0
    expected_price = 0
    get GET_TOTAL_PREFIX, {'quantity':1, "price":price,"state":"NV"}

    response = JSON.parse(last_response.body)
    expect(response['total_price']).to eq expected_price
  end

  it 'total de 1 unidad de precio unitario 100 en Nevada debe devolver 108 como total' do
    price = 100
    expected_price = 108
    get GET_TOTAL_PREFIX, {'quantity':1, "price":price,"state":"NV"}

    response = JSON.parse(last_response.body)
    expect(response['total_price']).to eq expected_price
  end

  it 'total de 1 unidad de precio unitario 1500 en Nevada debe devolver 1571.4 como total' do
    price = 1500
    expected_price = 1571.4
    get GET_TOTAL_PREFIX, {'quantity':1, "price":price,"state":"NV"}

    response = JSON.parse(last_response.body)
    expect(response['total_price']).to eq expected_price
  end

  it 'total de 2 unidades de precio unitario 100 en Nevada debe devolver 216 como total' do
    price = 100
    quantity = 2
    expected_price = 216
    get GET_TOTAL_PREFIX, {'quantity':quantity, "price":price,"state":"NV"}

    response = JSON.parse(last_response.body)
    expect(response['total_price']).to eq expected_price
  end

  it 'total de 1 unidad de precio unitario 100 en Utah debe devolver 106.85 como total' do
    price = 100
    quantity = 1
    state = 'UT'
    expected_price = 106.85
    get GET_TOTAL_PREFIX, {'quantity':quantity, "price":price,"state":state}

    response = JSON.parse(last_response.body)
    expect(response['total_price']).to eq expected_price
  end

  it 'total de 1 unidad de precio unitario 50000 en Alabama debe devolver 44200 como total' do
    price = 50000
    quantity = 1
    state = 'AL'
    expected_price = 44200
    get GET_TOTAL_PREFIX, {'quantity':quantity, "price":price,"state":state}

    response = JSON.parse(last_response.body)
    expect(response['total_price']).to eq expected_price
  end
end
