Elephant Carpaccio, proyecto base ruby
===========================


## Preparación del ambiente

sudo apt-get update
sudo apt-get install build-essential
sudo apt-get install -y git
gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3
curl -sSL https://get.rvm.io | bash -s stable
source ~/.rvm/scripts/rvm
rvm install 2.2.0
gem install bundler

## Uso del proyecto

Una vez realizada la instalación:

1. Instalar las dependencias del proyecto ejecutando _bundle install_
2. Ejecutar las pruebas ejecutando _bundle exec rake_


## Stories particionadas por iteración

Nota: Las stories no respetan el esquema "Como **X** quiero **Y** para **Z**" porque en
el ejercicio en clase no lo respetamos, pero sí expresan la funcionalidad deseada.

1. Calcular el importe total para una unidad de un producto de precio fijo, sin descuentos ni impuesto.
2. Calcular el importe total para una unidad de cualquier precio, sin descuentos ni impuesto.
3. Aplicar un descuento de 3% a un importe total si supera los U$S 1000.
4. Aplicar una tasa de impuesto correspondiente a Nevada.
5. Calcular el importe total para una cantidad arbitraria de unidades de un producto.
6. Aplicar una tasa de impuesto para cada provincia.
7. Aplicar un descuento definido en función de distintos rangos de importes totales.

* Iteración 1: stories 1, 2 y 4
* Iteración 2: stories 3 y 5
* Iteración 3: stories 6 y 7

## Uso del servicio

Se provee el endpoint **/get_total** al cual se debe llamar con parámetros:
* *price*: precio unitario del producto
* *quantity*: cantidad de unidades compradas
* *state*: primeras dos letras identificatorias del estado en el que se efectúa la compra.

Devuelve un objeto JSON con un campo *total_price* que contiene el precio total a pagar.
