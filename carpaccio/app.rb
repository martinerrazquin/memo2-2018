require 'sinatra'
require 'json'
require_relative 'model/discount_calculator'

GET_TOTAL_PREFIX = '/get_total'

get GET_TOTAL_PREFIX do
  unit_price = params['price'].to_f
  quantity = params['quantity'].to_i
  state = params['state']

  discount_calculator = DiscountCalculator.new

  total_price = discount_calculator.calculate_total(unit_price,quantity,state)

  content_type :json
  {'total_price': total_price}.to_json
end
